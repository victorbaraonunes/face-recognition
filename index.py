# https://github.com/opencv/opencv/blob/master/samples/python/facedetect.py

import numpy as np
import cv2

print("running on OpenCV " + cv2.__version__)

face_cascade = cv2.CascadeClassifier('./classifiers/haarcascade_frontalface_alt.xml')

weights = [1, 2, 4, 8, 16, 32, 64, 128]
faces = []

# set the webcam as the video source
# can also accept a video file path
capture = cv2.VideoCapture(0)
capture.set(3, 800)
capture.set(4, 600)

def thresholded(center, pixels):
	out = []
	for a in pixels:
		if a >= center:
			out.append(1)
		else:
			out.append(0)
	return out

def get_pixel_else_0(l, idx, idy, default=0):
    try:
        return l[idx,idy]
    except IndexError:
        return default

def compute_lbp(frame):
    lbp = frame.copy()
    for x in range(0, len(frame)):
        for y in range(0, len(frame[0])):
            center = frame[x,y]
            top_left = get_pixel_else_0(frame, x-1, y-1)
            top_up = get_pixel_else_0(frame, x, y-1)
            top_right = get_pixel_else_0(frame, x+1, y-1)
            right = get_pixel_else_0(frame, x+1, y)
            left = get_pixel_else_0(frame, x-1, y)
            bottom_left = get_pixel_else_0(frame, x-1, y+1)
            bottom_right = get_pixel_else_0(frame, x+1, y+1)
            bottom_down = get_pixel_else_0(frame, x, y+1)

            values = thresholded(center, [top_left, top_up, top_right, right, bottom_right, bottom_down, bottom_left, left])
            
            res = 0
            for a in range(0, len(values)):
                res += weights[a] * values[a]
            lbp.itemset((x, y), res)
    return lbp

while(True):
	# reads one frame from the video
	# ret = tells us if we run out of frames
	ret, frame = capture.read()

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.equalizeHist(gray)

	rects = face_cascade.detectMultiScale(
		gray
		, scaleFactor=1.2
		, minNeighbors=5)

	for (x, y, w, h) in rects:
		faces.append((x, y, w, h))

		lbp = compute_lbp(gray[y:y + h, x:x + w])

		cv2.putText(frame, 'Face %s' % (len(faces)), (x, y - 10), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)
		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

	cv2.imshow('camera', frame)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

capture.release()
cv2.destroyAllWindows()
